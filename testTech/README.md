Le projet fonctionne avec une version de Python >= 3.6 et Django 3.0

Dans un terminal executer les lignes de commandes suivantes :

sudo apt install python3-pip

pip3 install django==3.0

Pour lancer le serveur, aller à la racine du projet dans le répertoire 'testTech' puis executer la commande :

python3 manage.py runserver

Ensuite lancer un navigateur internet et écrire dans l'URL :

http://127.0.0.1:8000/worldMap/

Vous allez arriver sur la page de l'application contenant une carte du monde. Il y a 4 marqueurs dont les coordonnées sont stockées dans la base de données.
Les quatre marqueurs sont sur New York, Paris, Shangai et Cape Town.
Une fois terminé appuyer sur CTRL+C dans le terminal pour arrêter le serveur.
