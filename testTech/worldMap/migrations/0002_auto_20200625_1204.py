from django.db import migrations

# Second migration file which fill the DB with 4 markers on the map : Paris, New York, Shangaï and Cape Town
def loadDB(apps, schema_editor):
    Coords = apps.get_model('worldMap', 'Coords')
    coordsNY = Coords(latitude= 40.779897, longitude= -73.968565)
    coordsNY.save()
    coordsParis = Coords(latitude= 48.856614, longitude= 2.352222)
    coordsParis.save()
    coordsShangai = Coords(latitude= 31.230390, longitude= 121.473702)
    coordsShangai.save()
    coordsCT = Coords(latitude= -33.924869, longitude= 18.424055)
    coordsCT.save()


class Migration(migrations.Migration):

    dependencies = [
        ('worldMap', '0001_initial'),
    ]

    # Call the loadDB function above to fill the DataBase

    operations = [
        migrations.RunPython(loadDB),
    ]
