from django.http import HttpResponse
from django.shortcuts import render
from .models import Coords
from django.http import JsonResponse
import json
import os


# Home view with the map. The only view disponible
def home(request):
    # We save the coodinates data from DB in the variable markers
    markers = Coords.objects.all()
    # We put them in context and name them markers
    context = {'markers': markers}
    # We render the HTML page and we can access to the data with the word 'markers' thanks to context in parameters
    return render(request, 'worldMap/testD3.html', context)

def getJson(request):
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    my_file = os.path.join(THIS_FOLDER, 'templates/worldMap/departments.json')
    data = open(my_file).read()
    json_data = json.loads(data)

    return JsonResponse(json_data, safe=False)
