from django.urls import path, include

from . import views


app_name = 'worldMap'

# Url Patterns for the worldMap App

urlpatterns = [
    path('', views.home, name='home'),
    path('api/', views.getJson, name='api'),
]
