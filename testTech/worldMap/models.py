from django.db import models
from django.db.models import Model

# The model for the gps coordinates.

class Coords(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
